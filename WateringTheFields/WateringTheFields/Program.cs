﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WateringTheFields
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("В какой стране вы хотите начать свои посадки?");
            string country = Console.ReadLine();
            var c = new Country(country);
            bool isExit = false;
            Console.Clear();
            while (!isExit)
            {
                Console.WriteLine("\t\t {0}",country);
                Console.WriteLine("1 - Добавить поле");
                Console.WriteLine("2 - Добавить культуру на поле");
                Console.WriteLine("3 - Удалить последнее поле");
                Console.WriteLine("4 - Удалить культуру с поля");
                Console.WriteLine("5 - Изменить культуру");
                Console.WriteLine("6 - Поиск культуры");
                Console.WriteLine("7 - Выход");
                Console.Write("Выбрать: ");
                int select = Convert.ToInt32(Console.ReadLine());
                switch (select)
                {
                    case 1:
                        Console.Write("Какое количество полей вы хотите добавить: ");
                        int countLea = Convert.ToInt32(Console.ReadLine());
                        for(int i = 0; i < countLea; i++)
                        {
                            c.AddLea();
                        }
                        Console.Clear();
                        break;
                    case 2:
                        c.AddCultur();
                        Console.Clear();
                        break;
                    case 3:
                        c.RemoveLastLea();
                        Console.WriteLine(c.GetCapacity());
                        Console.ReadKey();
                        Console.Clear(); break;
                    case 4:
                        Console.Write("Введите название культуры: ");
                        string cultureRemove = Console.ReadLine();
                        c.Remove(cultureRemove);
                        Console.WriteLine(c.GetCapacity());
                        Console.ReadKey();
                        Console.Clear(); break;
                    case 5:
                        c.UpdateCultur();
                        Console.Clear();
                        break;
                    case 6:
                        Console.Write("Введите название культуры: ");
                        string cultureSearch = Console.ReadLine();
                        c.Search(cultureSearch);
                        Console.ReadKey();
                        Console.Clear();
                        break;
                    case 7: isExit = true; break;
                    default:
                        Console.Clear();
                        Console.WriteLine("Введено неверное значение!"); break;
                }
            }
        }
    }
}