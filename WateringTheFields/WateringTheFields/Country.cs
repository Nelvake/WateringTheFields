﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WateringTheFields
{
    class Country
    {
        private const int EMPTY = 0;
        private string _NameCountry { get; set; }
        private Lea[] _lea;
        private int _capacity;
        public Country(string NameCountry)
        {
            _NameCountry = NameCountry;
            _capacity = EMPTY;
            _lea = new Lea[_capacity];
        }
        public int GetCapacity()
        {
            return _capacity;
        }
        public void AddLea()
        {
            Array.Resize(ref _lea, ++_capacity);
            _lea[_capacity - 1] = new Lea();
        }
        public void RemoveLastLea()
        {
            if(_capacity == EMPTY)
            {
                return;
            }
            _lea = _lea.Take(_lea.Count() - 1).ToArray();
            --_capacity;
        }
        public void Remove(string nameCulture)
        {
            for (int i = 0; i < _lea.Length; i++)
            {
                if (_lea[i]._NameCulture == nameCulture)
                {
                    _lea[i] = new Lea();
                    return;
                }
            }
        }
        public void AddCultur()
        {
            int select;
            Console.Write("На какое поле вы хотите засадить культуру?: ");
            select = Convert.ToInt32(Console.ReadLine());
            if(select - 1 > _capacity - 1 || select < 0)
            {
                Console.WriteLine("Введено неверное поле!");
                Console.ReadKey();
                return;
            }
            Console.Write("Введите название культуры: ");
            string NameCulture = Console.ReadLine();
            _lea[select - 1]._NameCulture = NameCulture;
            Console.Write("Введите план полива: ");
            string IrrigationPlan = Console.ReadLine();
            _lea[select - 1]._IrrigationPlan = IrrigationPlan;
        }
        public void UpdateCultur()
        {
            int select;
            Console.Write("На каком поле вы хотите изменить культуру: ");
            select = Convert.ToInt32(Console.ReadLine());
            if (select - 1 > _capacity - 1 || select < 0)
            {
                Console.WriteLine("Введено неверное поле!");
                Console.ReadKey();
                return;
            }
            Console.Write("Введите название культуры: ");
            string NameCulture = Console.ReadLine();
            _lea[select - 1]._NameCulture = NameCulture;
            Console.WriteLine("Хотите изменить план полива для этой культуры?(y,n)");
            string selectWater = Console.ReadLine();
            if (selectWater == "y")
            {
                Console.Write("Введите план полива: ");
                string IrrigationPlan = Console.ReadLine();
                _lea[select - 1]._IrrigationPlan = IrrigationPlan;
            }
            return;
        }
        public void Search(string nameCulture)
        {
            for(int i = 0; i < _capacity; i++)
            {
                if(_lea[i]._NameCulture == nameCulture)
                {
                    Console.WriteLine("{0} расположена на {1} поле ",nameCulture,i+1);
                    Console.Write("План полива для данной культуры: " + _lea[i]._IrrigationPlan + " в день");
                    return;
                }
            }
            Console.WriteLine("Культура не найдена!");
        }
    }
}
